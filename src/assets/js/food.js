$(function () {

    // TODO: Implement meal templates

    var foodsEaten = [];
    var exerciseCalories = 0;
    init();

    /** Sets up styling and event handler bindings, and perform initial data load from the server. */
    function init() {
        // Initialize the Datepicker widget
        $("#datepicker").val(parseCurrentDate());
        $("#datepicker").change(function() {
            loadFoodsEaten($('#datepicker').val());
            loadRecentFoods($('#datepicker').val());
        });

        // Event handling to add foods eaten when the "Recent Foods" add button is clicked
        $("#recentFoodsButton").click(function () {
            var foodId = $("#recentFoodsSelect").val();
            addFoodEaten(foodId);
        });

        // Event handling to launch the search modal when the "Search Foods" button is clicked (or the Enter key
        // pressed while the search box has focus)
        $("#searchFoodsButton").click(function () {
            searchFoods();
        });
        $("#searchFoodName").keypress(function(e) {
            if (e.which == 13) {
                searchFoods();
            }
        });

        // Event handling to launch the create/edit foods modal when the "Create New Food" button is clicked
        $('#createFoodButton').click(function () {
            launchCreateFoodModal();
        });

        // Event handling for the submit button in the create/edit foods modal
        $("#editFoodSubmitButton").click(function () {
            editFoodSubmit();
        });

        // Initial data load from server
        loadFoodsEaten(parseCurrentDate());
        loadRecentFoods(parseCurrentDate());
    }

    /** Retrieves information from the server about foods eaten on the currently selected date. */
    function loadFoodsEaten(dateString) {
        var foodsEatenPayload;
        var exercisesPerformedPayload;
        $.when(
            $.ajax({
                method: 'GET',
                url: '/api/foodeaten/' + encodeURIComponent(dateString)
            }).done(function(payload) {
                foodsEatenPayload = payload;
            }).fail(function(jqXHR, textStatus) {
                console.log('GET /api/foodeaten/' + dateString + ' request failed: ' + jqXHR.status);
                if (jqXHR.status == 401) {
                    logout();
                }
            }),

            $.ajax({
                method: 'GET',
                url: '/api/exerciseperformed/' + encodeURIComponent(dateString)
            }).done(function(payload) {
                exercisesPerformedPayload = payload;
            }).fail(function(jqXHR, textStatus) {
                console.log('GET /api/exerciseperformed/' + dateString + ' request failed: ' + jqXHR.status);
                if (jqXHR.status == 401) {
                    logout();
                }
            })
        ).then(function() {
            // success
            foodsEaten = foodsEatenPayload;

            exerciseCalories = 0;
            for (var index = 0; index < exercisesPerformedPayload.length; index++) {
                exerciseCalories += exercisesPerformedPayload[index].caloriesBurned;
            }

            renderFoodsEaten();
        }, function () {
            // failure
            console.log('An error occurred retrieving food eaten or exercise performed data for this date');
        });

    }

    /** Populates the foods eaten table with the data retrieved by "loadFoodsEaten(...)" */
    function renderFoodsEaten() {
        function roundDouble(number) {
            return number % 1 == 0 ? number : number.toFixed(2);
        }
        function buildOption(actualValue, expectedValue) {
            return '<option value="' + expectedValue + '" ' + (expectedValue.toUpperCase() == actualValue ? 'selected="selected"' : '') + '>' + expectedValue + '</option>';
        }
        $('#foodsEaten tbody').empty();
        $('#foodsEaten tfoot').empty();
        var totalCalories = 0;
        var totalFat = 0;
        var totalSaturatedFat = 0;
        var totalSodium = 0;
        var totalCarbs = 0;
        var totalFiber = 0;
        var totalSugar = 0;
        var totalProtein = 0;
        var sortedFoodsEaten = foodsEaten.sort(function(a, b) {
            if (a.food.name < b.food.name) {
                return -1;
            }
            if (a.food.name > b.food.name) {
                return 1;
            }
            return 0;
        });
        for (var index = 0; index < sortedFoodsEaten.length; index++) {
            var foodEaten = sortedFoodsEaten[index];

            totalCalories += foodEaten.calories;
            totalFat += foodEaten.fat;
            totalSaturatedFat += foodEaten.saturatedFat;
            totalSodium += foodEaten.sodium;
            totalCarbs += foodEaten.carbs;
            totalFiber += foodEaten.fiber;
            totalSugar += foodEaten.sugar;
            totalProtein += foodEaten.protein;
            var servingTypeOptions = '';
            if (foodEaten.servingType == 'CUSTOM') {
                servingTypeOptions = '<option value="CUSTOM">CUSTOM</option>';
            } else {
                servingTypeOptions += buildOption(foodEaten.servingType, 'ounce');
                servingTypeOptions += '<option value="cup" ' + (foodEaten.servingType == 'CUP' ? 'selected="selected"' : '') + '>cup</option>';
                servingTypeOptions += '<option value="pound" ' + (foodEaten.servingType == 'POUND' ? 'selected="selected"' : '') + '>pound</option>';
                servingTypeOptions += '<option value="pint" ' + (foodEaten.servingType == 'PINT' ? 'selected="pint"' : '') + '>pint</option>';
                servingTypeOptions += '<option value="tablespoon" ' + (foodEaten.servingType == 'TABLESPOON' ? 'selected="tablespoon"' : '') + '>tablespoon</option>';
                servingTypeOptions += '<option value="teaspoon" ' + (foodEaten.servingType == 'TEASPOON' ? 'selected="teaspoon"' : '') + '>teaspoon</option>';
                servingTypeOptions += '<option value="gram" ' + (foodEaten.servingType == 'gram' ? 'selected="GRAM"' : '') + '>gram</option>';
            }
            $('#foodsEaten tbody').append('<tr id="' + foodEaten.id + '">'
                + '<td width="30%">' + foodEaten.food.name + '</td>'
                + '<td><form class="uk-form"><input type="text" size="2" value="' + foodEaten.servingQty + '"/></form></td>'
                + '<td><form class="uk-form"><select>' + servingTypeOptions + '</select></form></td>'
                + '<td>' + Math.round(foodEaten.calories) + '</td>'
                + '<td>' + roundDouble(foodEaten.fat) + '</td>'
                + '<td>' + roundDouble(foodEaten.saturatedFat) + '</td>'
                + '<td>' + roundDouble(foodEaten.sodium) + '</td>'
                + '<td>' + roundDouble(foodEaten.carbs) + '</td>'
                + '<td>' + roundDouble(foodEaten.fiber) + '</td>'
                + '<td>' + roundDouble(foodEaten.sugar) + '</td>'
                + '<td>' + roundDouble(foodEaten.protein) + '</td>'
                + '<td><a href="#"><i class="uk-icon-trash"/></a></td>'
                + '</tr>'
            );
            var servingQtyInput = $('#' + foodEaten.id).find('input');
            var servingTypeInput = $('#' + foodEaten.id).find('select');
            var deleteButton = $('#' + foodEaten.id).find('i.uk-icon-trash');
            servingQtyInput.on('change', servingQtyInput, updateFoodEaten);
            servingTypeInput.on('change', servingTypeInput, updateFoodEaten);
            deleteButton.on('click', deleteButton, deleteFoodEaten);
        }
        var netCalories = totalCalories - exerciseCalories;
        $('#foodsEaten tfoot').append('<tr>'
            + '<td width="30%">TOTAL:</td><td><br/>&nbsp;</td><td><br/>&nbsp;</td>'
            + '<td>' + Math.round(totalCalories) + '<br/>' + Math.round(netCalories) + ' net cal.<br/></td>'
            + '<td>' + roundDouble(totalFat) + '<br/></td>'
            + '<td>' + roundDouble(totalSaturatedFat) + '<br/></td>'
            + '<td>' + roundDouble(totalSodium) + '<br/></td>'
            + '<td>' + roundDouble(totalCarbs) + '<br/></td>'
            + '<td>' + roundDouble(totalFiber) + '<br/></td>'
            + '<td>' + roundDouble(totalSugar) + '<br/></td>'
            + '<td>' + roundDouble(totalProtein) + '<br/></td>'
            + '<td></td>'
            + "</tr>"
        );
    }

    /** Adds a new food eaten for the currently selected date. */
    function addFoodEaten(foodId) {
        var dateString = $('#datepicker').val();
        $.ajax({
            method: 'POST',
            url: '/api/foodeaten',
            contentType: 'application/json',
            data: JSON.stringify({ id: foodId, date: dateString }),
            async: false
        }).done(function(foodEatenPayload) {
            foodsEaten.push(foodEatenPayload);
            renderFoodsEaten();
        }).fail(function(jqXHR, textStatus) {
            console.log('An error occurred adding a food eaten: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
        });
    }

    /** Update an existing food eaten, when its serving type or qty is changed in the foods eaten table. */
    function updateFoodEaten(event) {
        var input = event.data;
        var id = $(input).parent().parent().parent().attr('id');
        var servingQty = $(input).parent().parent().parent().find('input').val();
        var servingType = $(input).parent().parent().parent().find('select').val();
        $.ajax({
            method: 'PUT',
            url: '/api/foodeaten/' + encodeURIComponent(id),
            contentType: 'application/json',
            data: JSON.stringify({ servingType: servingType, servingQty: servingQty })
        }).done(function(updatedFoodEaten) {
            for (var index = 0; index < foodsEaten.length; index++) {
                if (foodsEaten[index].id == updatedFoodEaten.id) {
                    foodsEaten[index] = updatedFoodEaten;
                }
            }
            $(input).css('border', '2px solid green');
            setTimeout(function() {
                $(input).css('border', '0.666667px solid rgb(223, 215, 202');
                renderFoodsEaten();
            }, 1000);
        }).fail(function(jqXHR, textStatus) {
            console.log('An error occurred updating a food eaten: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
            $(input).css('border', '2px solid red');
        });
    }

    /** Delete an existing food eaten, when it is removed from the foods eaten table. */
    function deleteFoodEaten(event) {
        var input = event.data;
        var id = $(input).parent().parent().parent().attr('id');
        $.ajax({
            method: 'DELETE',
            url: '/api/foodeaten/' + encodeURIComponent(id)
        }).done(function() {
            for (var index = 0; index < foodsEaten.length; index++) {
                if (foodsEaten[index].id == id) {
                    foodsEaten.splice(index, 1);
                }
            }
            $(input).parent().parent().parent().css('border', '2px solid green');
            setTimeout(function() {
                $(input).parent().parent().parent().remove();
                renderFoodsEaten();
            }, 500);
        }).fail(function(jqXHR, textStatus) {
            console.log('An error occurred deleting a food eaten: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
            $(input).parent().parent().parent().css('border', '2px solid red');
        });
    }

    /**
     * Populates the "Recently Eaten Foods" select pull-down, when the page is first loaded and when the date
     * is changed.
     */
    function loadRecentFoods(dateString) {
        $.ajax({
            method: 'GET',
            url: '/api/food/recent/' + encodeURIComponent(dateString)
        }).done(function (recentFoodsPayload) {
            $('#recentFoodsSelect').empty();
            for (var index = 0; index < recentFoodsPayload.length; index++) {
                var recentFood = recentFoodsPayload[index];
                $('#recentFoodsSelect').append('<option value="' + recentFood.id + '">' + recentFood.name + '</option>')
            }
        }).fail(function(jqXHR, textStatus) {
            console.log('An error occurred when loading recently eaten foods: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
        });
    }

    /** Displays a modal with search results. */
    function searchFoods() {
        var searchString = $('#searchFoodName').val();
        $.ajax({
            url: '/api/food/search/' + encodeURIComponent(searchString),
            dataType: 'json',

            success: function (searchResults) {
                $('#searchFoodsTable').empty();
                $('#searchFoodsTable').append("<thead><tr><th>Name</th><th>Default Serving</th><th>Calories</th><th>Fat</th><th>Carbs</th><th></th></tr></thead>");
                for (var index = 0; index < searchResults.length; index++) {
                    var food = searchResults[index];
                    var foodRow = "<tr><td><a id='edit" + food.id + "' title='Click to edit this food' href='#'>" + food.name + "</a></td>";
                    foodRow += '<td>' + parseFloat(food.servingTypeQty.toFixed(2)) + ' ' + food.defaultServingType.toLowerCase() + '</td>';
                    foodRow += '<td>' + food.calories + '</td>';
                    foodRow += '<td>' + parseFloat(food.fat.toFixed(2)) + '</td>';
                    foodRow += '<td>' + parseFloat(food.carbs.toFixed(2)) + '</td>';
                    foodRow += "<td><a href='#'><i id='search" + food.id + "' class='uk-icon-plus'/></a></td></tr>";
                    $('#searchFoodsTable').append(foodRow);

                    $('#edit' + food.id).click(function (event) {
                        var id = $(event.target).attr('id').replace(/^edit/, '');
                        UIkit.modal('#searchFoodsModal').hide();
                        launchEditFoodModal(id);
                    });
                    $('#search' + food.id).click(function(event) {
                        var id = $(event.target).attr('id').replace(/^search/, '');
                        addFoodEaten(id);
                        UIkit.modal('#searchFoodsModal').hide();
                    });
                }
                UIkit.modal('#searchFoodsModal').show();
            },

            fail: function (jqXHR, textStatus) {
                console.log('An error occurred in search: ' + jqXHR.status);
                if (jqXHR.status == 401) {
                    logout();
                }
            }
        });
    }

    /** Called when a food's name is clicked in the Search Foods modal. */
    function launchEditFoodModal(foodId) {
        $.ajax({
            method: 'GET',
            url: '/api/food/' + encodeURIComponent(foodId),
            dataType: 'json',

            success: function (food) {
                if (food.global) {
                    $('#editFoodModalTitle').text('Create Customized Food');
                    $('#editFoodMessage').html('<p>You are about to create a private customized copy of a food from the global list.</p><p>This will NOT effect any existing data entries where you ate global version previously.</p>');
                } else {
                    $('#editFoodModalTitle').text('Edit Food');
                    $('#editFoodMessage').html('<p>You are about to edit a food from your private customized list.</p><p>This WILL effect any existing data entries where you ate it previously.</p>');
                }
                $("#editFoodServingType").empty();
                if (food.defaultServingType.toUpperCase() === 'CUSTOM') {
                    $("#editFoodServingType").append("<option value='CUSTOM'>CUSTOM</option>");
                } else {
                    var servingTypes = ['OUNCE', 'CUP', 'POUND', 'PINT', 'TABLESPOON', 'TEASPOON', 'GRAM'];
                    for (var index = 0; index < servingTypes.length; index++) {
                        if (food.defaultServingType.toUpperCase() === servingTypes[index]) {
                            $('#editFoodServingType').append("<option value='" + servingTypes[index] + "' selected='selected'>" + servingTypes[index].toLowerCase() + "</option>");
                        } else {
                            $('#editFoodServingType').append("<option value='" + servingTypes[index] + "'>" + servingTypes[index].toLowerCase() + "</option>");
                        }
                    }
                }
                $('#editFoodId').val(food.id);
                $('#editFoodIsPost').val(food.global);
                $('#editFoodName').val(food.name);
                $('#editFoodServingQty').val(food.servingTypeQty);
                $('#editFoodCalories').val(food.calories);
                $('#editFoodFat').val(food.fat);
                $('#editFoodSaturatedFat').val(food.saturatedFat);
                $('#editFoodCarbs').val(food.carbs);
                $('#editFoodFiber').val(food.fiber);
                $('#editFoodSugar').val(food.sugar);
                $('#editFoodProtein').val(food.protein);
                $('#editFoodSodium').val(food.sodium);

                UIkit.modal('#editFoodModal').show();
            },

            fail: function (jqXHR, textStatus) {
                alert('Request failed: ' + jqXHR.status);
                if (jqXHR.status == 401) {
                    logout();
                }
            }
        });
    }

    /** Called when the submit button is clicked in the Add or Edit Food modal. */
    function editFoodSubmit() {
        if ($('#editFoodIsPost').val() === 'true') {
            var method = 'POST';
        } else {
            var method = 'PUT';
        }
        $.ajax({
            url: '/api/food',
            method: method,
            data: {
                id: $('#editFoodId').val(),
                name: $('#editFoodName').val(),
                defaultServingType: $('#editFoodServingType').val(),
                servingTypeQty: $('#editFoodServingQty').val(),
                calories: $('#editFoodCalories').val(),
                fat: $('#editFoodFat').val(),
                saturatedFat: $('#editFoodSaturatedFat').val(),
                carbs: $('#editFoodCarbs').val(),
                fiber: $('#editFoodFiber').val(),
                sugar: $('#editFoodSugar').val(),
                protein: $('#editFoodProtein').val(),
                sodium: $('#editFoodSodium').val()
            },

            success: function (data, textStatus, jqXHR) {
                alert(data);
                UIkit.modal('#editFoodModal').hide();
            },

            fail: function (jqXHR, textStatus, errorThrown) {
                alert('Request failed: ' + jqXHR.status);
                if (jqXHR.status == 401) {
                    logout();
                }
            }
        });
    }

    /** Called when the "Create New Food" button is clicked. */
    function launchCreateFoodModal() {
        $('#editFoodModalTitle').text('Create New Food');
        $('#editFoodMessage').text('You are creating a brand-new food.');

        $('#editFoodServingType').text('');
        var servingTypes = ['OUNCE', 'CUP', 'POUND', 'PINT', 'TABLESPOON', 'TEASPOON', 'GRAM'];
        for (var index = 0; index < servingTypes.length; index++) {
            $("#editFoodServingType").append("<option value='" + servingTypes[index] + "'>" + servingTypes[index].toLowerCase() + "</option>");
        }
        $("#editFoodServingType").append("<option value='CUSTOM'>CUSTOM</option>");

        $('#editFoodId').val('');
        $('#editFoodIsPost').val('true');
        $('#editFoodName').val('');
        $('#editFoodServingQty').val('');
        $('#editFoodCalories').val('');
        $('#editFoodFat').val('');
        $('#editFoodSaturatedFat').val('');
        $('#editFoodCarbs').val('');
        $('#editFoodFiber').val('');
        $('#editFoodSugar').val('');
        $('#editFoodProtein').val('');
        $('#editFoodSodium').val('');

        UIkit.modal('#editFoodModal').show();
    }

});

