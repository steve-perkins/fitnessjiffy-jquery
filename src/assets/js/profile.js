$(function () {

    // TODO: Allow changing email address, but include warning (and server-side check) for Google users
    // TODO: Implement progress "badges"

    init();

    /** Sets up styling and event handler bindings, and perform initial data load from the server. */
    function init() {
        // Initialize the datepicker widget
        $("#datepicker-weight").val(parseCurrentDate());
        $("#datepicker-weight").change(function() {
            loadWeight($('#datepicker-weight').val());
        });

        // Event handler to save the weight when the save button is clicked or the Enter key is pressed in that field
        $('#weightEntry').keypress(function (e) {
            var key = e.which;
            if(key == 13) {
                saveWeight($('#datepicker-weight').val());
            }
        });
        $('#weightEntrySubmit').click(function() {
            saveWeight($('#datepicker-weight').val());
        });

        // Event handler to send updated profile information to the server when the "Update Profile" button is clicked
        $('#profileSubmit').click(function() {
            saveProfile();
        });

        // Event handler to change the password when the button is clicked.
        $('#passwordSubmit').click(function() {
            savePassword();
        });

        loadWeight(parseCurrentDate());
        loadProfile();
    }

    /** Retrieves from the server the user's weight on the given date. */
    function loadWeight(dateString) {
        $.ajax({
            url: '/api/user/weight/' + encodeURIComponent(dateString)
        }).done(function(weight) {
            $('#weightEntry').val(weight);
        }).fail(function(jqXHR, textStatus) {
            console.log('GET /api/user/weight/' + dateString + ' request failed: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
        });
    }

    /** Updates on the server the user's weight on the given date. */
    function saveWeight(dateString) {
        clearFlashMessages();
        var data = {};
        data['weight'] = $('#weightEntry').val();
        $.ajax({
            method: 'POST',
            contentType: 'application/json',
            url: '/api/user/weight/' + encodeURIComponent(dateString),
            data: JSON.stringify(data)
        }).done(function(weight) {
            loadProfile();
        }).fail(function(jqXHR, textStatus) {
            console.log('POST /api/user/weight/' + dateString + ' request failed: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
        });
    }

    /** Retrieves from the server the user's profile information. */
    function loadProfile() {
        // Load user profile
        $.ajax({
            url: '/api/user'
        }).done(function(user) {
            $('#currentWeight').text(user.currentWeight.toFixed(1));
            $('#bmi').text(user.bmi.toFixed(2));
            $('#maintenanceCalories').text(user.maintenanceCalories);
            $('#email').val(user.email);
            $('#firstName').val(user.firstName);
            $('#lastName').val(user.lastName);

            if (user.gender == 'FEMALE') {
                $('#female').prop('checked', true);
            } else {
                $('#male').prop('checked', true);
            }

            $('#datepicker-birthdate').val(user.birthdate);

            var heightFeet = Math.floor(user.heightInInches / 12);
            $('#heightFeet').val(heightFeet);
            var heightInches = user.heightInInches % 12;
            $('#heightInches').val(heightInches);

            $('#activityLevel').val(user.activityLevel.toUpperCase());
            $('#timeZone').val(user.timeZone);
        }).fail(function(jqXHR, textStatus) {
            console.log('GET /api/user request failed: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
        });

    }

    /** Event handler to save the profile when the save button is clicked. */
    function saveProfile() {
        clearFlashMessages();

        var data = {};
        data['firstName'] = $('#firstName').val();
        data['lastName'] = $('#lastName').val();
        data['gender'] = $('input[name=gender]:checked').val();
        data['birthdate'] = $('#datepicker-birthdate').val();
        data['heightInInches'] = parseInt($('#heightFeet').val()) * 12 + parseInt($('#heightInches').val());
        data['activityLevel'] = $('#activityLevel').val();
        data['timeZone'] = $('#timeZone').val();

        $.ajax({
            method: 'PUT',
            contentType: 'application/json',
            url: '/api/user',
            data: JSON.stringify(data)
        }).done(function() {
            loadProfile();
            $('#profileSuccessRow').show();
        }).fail(function(jqXHR, textStatus) {
            console.log('PUT /api/user request failed: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
            $('#profileErrorMessage').text('An error occurred');
            $('#profileErrorRow').hide();
        });
    }

    /** Event handler to change the password when the button is clicked. */
    function savePassword() {
        clearFlashMessages();

        var data = {};
        data['currentPassword'] = $('#currentPassword').val();
        data['newPassword'] = $('#newPassword').val();
        data['reenterNewPassword'] = $('#reenterNewPassword').val();

        $.ajax({
            method: 'POST',
            contentType: 'application/json',
            url: '/api/user/password',
            data: JSON.stringify(data)
        }).done(function(msg) {
            $('#passwordSuccessRow').show();
        }).fail(function(jqXHR, textStatus) {
            console.log('POST /api/user/password request failed: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
            $('#passwordErrorMessage').text(jqXHR.responseText);
            $('#passwordErrorRow').show();
        });
    }

    /** Utility function to clear out any errors or status messages (e.g. "You've been logged out"). */
    function clearFlashMessages() {
        $('#profileSuccessRow').hide();
        $('#profileErrorRow').hide();
        $('#passwordSuccessRow').hide();
        $('#passwordErrorRow').hide();
    }

});

