$(function() {

    // TODO: Add axis criteria for macros (and micros?), and their moving averages
    // TODO: Reorganize the stats summary section
    // TODO: Add pie chart for macros

    var FIELD_LAST_MONTH = "30";
    var FIELD_LAST_YEAR = "365";
    var FIELD_ALL_DATES = "all";
    var FIELD_WEIGHT = "pounds";
    var FIELD_NET_CALORIES = "netCalories";
    var FIELD_CALORIES_30AVG = "calories30avg";

    var LABEL_LAST_MONTH = "Most recent month";
    var LABEL_LAST_YEAR = "Most recent year";
    var LABEL_ALL_DATES = "All dates";
    var LABEL_WEIGHT = "Weight";
    var LABEL_NET_CALORIES = "Net calories per day";
    var LABEL_CALORIES_30AVG = "Calories 30-day moving average";

    var chartData;
    init();

    /** Sets up event handler bindings, and perform initial data load from the server. */
    function init() {
        // Add event handling to the chart criteria selects
        $("#dateRange").change(function () {
            update();
        });
        $("#leftAxis").change(function () {
            update();
        });
        $("#rightAxis").change(function () {
            update();
        });

        loadReportData();
    }

    /** Retrieves report data from the server, and calls functions to render the chart and stats summary. */
    function loadReportData() {
        var limit = '';
        if ($("#dateRange").val() === FIELD_LAST_MONTH) {
            limit = '?limit=30';
        } else if ($("#dateRange").val() === FIELD_LAST_YEAR) {
            limit = '?limit=365';
        }

        return $.ajax({
            method: 'GET',
            url: '/api/report' + limit,
        }).done(function(response) {
            chartData = response;
            renderChart(FIELD_WEIGHT, FIELD_NET_CALORIES);
            renderStats();
        }).fail(function(jqXHR, textStatus) {
            console.log('An error occurred when loading report data: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
        });
    }

    /** Configures the amCharts widget based on the currently left and right axis selections. */
    function renderChart(leftAxis, rightAxis) {
        AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "none",
            "pathToImages": "http://www.amcharts.com/lib/3/images/",
            "legend": {
                "useGraphSettings": true
            },
            "dataProvider": chartData,
            "valueAxes": [{
                "id":"v1",
                "axisColor": "#FF6600",
                "axisThickness": 2,
                "gridAlpha": 0,
                "axisAlpha": 1,
                "position": "right"
            }, {
                "id":"v2",
                "axisColor": "#FCD202",
                "axisThickness": 2,
                "gridAlpha": 0,
                "axisAlpha": 1,
                "position": "left"
            }],
            "graphs": [{
                "valueAxis": "v1",
                "lineColor": "#FCD202",
                "bullet": "square",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 0,
                "title": fieldToLabel(rightAxis),
                "valueField": rightAxis,
                "fillAlphas": 0
            }, {
                "valueAxis": "v2",
                "lineColor": "#FF6600",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 0,
                "title": fieldToLabel(leftAxis),
                "valueField": leftAxis,
                "fillAlphas": 0
            }],
            "chartScrollbar": {},
            "chartCursor": {
                "cursorPosition": "mouse"
            },
            "categoryField": "date",
            "categoryAxis": {
                "parseDates": true,
                "axisColor": "#DADADA",
                "minorGridEnabled": true
            }
        });
    }

    /** Calculates stats summary and renders it in that section of the page. */
    function renderStats() {
        var startingWeight = chartData[0].pounds;
        var endingWeight = chartData[chartData.length - 1].pounds;
        var weightChange = endingWeight - startingWeight;
        var weightChangePerWeek = weightChange / (chartData.length / 7);
        var maxWeight = 0;
        var minWeight;
        var totalWeights = 0;
        var totalCalories = 0;
        for (var index = 0; index < chartData.length; index++) {
            var pounds = chartData[index].pounds;
            if (pounds > maxWeight) {
                maxWeight = pounds;
            }
            if (minWeight === undefined || pounds < minWeight) {
                minWeight = pounds;
            }
            totalWeights += pounds;
            totalCalories += chartData[index].netCalories;
        }
        var weightRange = maxWeight - minWeight;
        var averageWeight = Math.round(totalWeights / chartData.length);
        var averageCalories = Math.round(totalCalories / chartData.length);
        var label = fieldToLabel( $("#dateRange").val() );
        $("#statLabel").text(label);
        $("#startingWeight").text( limitToOneDecimalPlace(startingWeight).toString() );
        $("#endingWeight").text( limitToOneDecimalPlace(endingWeight).toString() );
        $("#maxWeight").text( limitToOneDecimalPlace(maxWeight).toString() );
        $("#minWeight").text( limitToOneDecimalPlace(minWeight).toString() );
        $("#weightChange").text( limitToOneDecimalPlace(weightChange).toString() );
        $("#weightChangePerWeek").text( limitToOneDecimalPlace(weightChangePerWeek).toString() );
        $("#averageWeight").text( limitToOneDecimalPlace(averageWeight).toString() );
        $("#weightRange").text( limitToOneDecimalPlace(weightRange).toString() );
        $("#averageCalories").text( limitToOneDecimalPlace(averageCalories).toString() );
    }

    /** Regenerates the chart and stats summary when the criteria selection changes. */
    function update() {
        var jqXHR = loadReportData();
        jqXHR.then(function () {
            if ($("#leftAxis").val() === FIELD_CALORIES_30AVG
                || $("#rightAxis").val() === FIELD_CALORIES_30AVG
            ) {
                verifyOrCreateMovingAverageData();
            }

            var leftAxis = $("#leftAxis").val();
            var rightAxis = $("#rightAxis").val();
            renderChart(leftAxis, rightAxis);
            renderStats();
        }, function(jqXHR, textStatus, errorThrown) {
            console.log('An error occurred when updating report data: ' + jqXHR.status);
        });
    }

    /**
     * Since moving averages are not calculated on the server, this function is called the user selects a
     * moving average as one of the chart axis settings.  The function iterates over the report data and
     * enriches it with 30-day moving averages.
     */
    function verifyOrCreateMovingAverageData() {
        // Check for whether the fields have already been added previously, or if there just isn't any data to work with anyway.
        if (chartData === undefined || chartData.length === 0 || chartData[0].calories30avg != undefined) {
            return;
        }
        var updatedData = [];
        for (var masterIndex = 0; masterIndex < chartData.length; masterIndex++) {
            var datesIncluded = 1;
            var totalCalories = chartData[masterIndex].netCalories;
            for (var elementIndex = (masterIndex >= 30) ? masterIndex - 30 : 0; elementIndex < masterIndex; elementIndex++) {
                datesIncluded++;
                totalCalories += chartData[elementIndex].netCalories;
            }
            var updatedElement = $.extend({}, chartData[masterIndex]);
            updatedElement.calories30avg = Math.round(totalCalories / datesIncluded);
            updatedData.push(updatedElement);
        }
        chartData = updatedData;
    }

    /**
     * Translate computer-readable field strings (e.g. <option> values) to human-readable display
     * text (e.g. <option> labels).
     */
    function fieldToLabel(field) {
        switch(field) {
            case FIELD_WEIGHT :
                return LABEL_WEIGHT;
                break;
            case FIELD_NET_CALORIES :
                return LABEL_NET_CALORIES;
                break;
            case FIELD_CALORIES_30AVG :
                return LABEL_CALORIES_30AVG;
                break;
            case FIELD_LAST_MONTH :
                return LABEL_LAST_MONTH;
                break;
            case FIELD_LAST_YEAR :
                return LABEL_LAST_YEAR;
                break;
            case FIELD_ALL_DATES :
                return LABEL_ALL_DATES;
                break;
            default :
                return "unknown";
                break;
        }
    }

    /**
     * Used for values to insert in the status summary section.  Returns the number as-is if is already
     * an integer, or has no more than one digit following the decimal point (e.g. "2", "10.7").
     * Otherwise (e.g. "4.978666666"), the number is truncated to have no more than one digit following
     * the decimal point ("4.9").
     */
    function limitToOneDecimalPlace(number) {
        return (number.toString().indexOf('.') >= 0) ? number.toFixed(1) : number;
    }

});

