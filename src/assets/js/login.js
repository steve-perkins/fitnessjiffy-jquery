$(function () {

    // Display a flash message after logging out
    if (getRequestParam('logout')) {
        $('#logoutRow').show();
    }

    // Event handlers to login when the submit button is clicked, or when the Enter key is pressed
    // in either the username or password fields
    $('#username').keypress(function (e) {
        var key = e.which;
        if(key == 13) {
            login();
        }
    });
    $('#password').keypress(function (e) {
        var key = e.which;
        if(key == 13) {
            login();
        }
    });
    $('#loginSubmit').click(function() {
        var username = $('#username').val();
        var password = $('#password').val();
        login(username, password);
    });
    $('#registerSubmit').click(function() {
        register();
    });

    function login(username, password) {
        $('#loginErrorRow').hide();
        $('#logoutRow').hide();
        var data = {};
        data['username'] = username;
        data['password'] = password;

        $.ajax({
            method: 'POST',
            contentType: 'application/json',
            url: '/api/auth/userpass',
            data: JSON.stringify(data)
        }).done(function(auth) {
            document.cookie = 'Authorization=local_' + auth.token + '; path=/';
            window.location = '/profile.html';
        }).fail(function(jqXHR, textStatus) {
            console.log('POST /api/auth/userpass request failed: ' + jqXHR.status);
            var auth = JSON.parse(jqXHR.responseText);
            $('#errorMessage').text(auth.error);
            $('#loginErrorRow').show();
        });
    }

    function register() {
        $('#registrationErrorRow').hide();

        var username = $('#registrationUsername').val();
        var password = $('#registrationPassword').val();
        var reenterPassword = $('#registrationReenterPassword').val();
        if (!username || !password || !reenterPassword) {
            $('#registrationErrorMessage').text('All three fields are required');
            $('#registrationErrorRow').show();
            return;
        } else if (password != reenterPassword) {
            $('#registrationErrorMessage').text('The "Password" and "Re-enter Password" fields must match');
            $('#registrationErrorRow').show();
            return;
        }

        var data = {};
        data['email'] = username;
        data['password'] = password;
        $.ajax({
            method: 'POST',
            contentType: 'application/json',
            url: '/api/user',
            async: false,
            data: JSON.stringify(data)
        }).done(function() {
            console.log('Registration complete for ' + username + '.  Logging in...');
            login(username, password);
        }).fail(function(jqXHR, textStatus) {
            console.log('POST /api/user request failed: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
            $('#registrationErrorMessage').text('An error occurred');
            $('#registrationErrorRow').show();
        });
    }
});

// Google Sign-In functions (must be in global scope)

function renderButton() {
    gapi.signin2.render('google-signin-button', {
        'scope': 'profile email',
        'width': 240,
        'height': 50,
        'longtitle': true,
        'theme': 'theme',
        'onsuccess': onLoginSuccess,
        'onfailure': onFailure
    });
    gapi.signin2.render('google-register-button', {
        'scope': 'profile email',
        'width': 240,
        'height': 50,
        'longtitle': true,
        'theme': 'theme',
        'onsuccess': onRegistrationSuccess,
        'onfailure': onFailure
    });
}

function onLoginSuccess(googleUser) {
    console.log('Logged in as: ' + googleUser.getBasicProfile().getEmail());
    var idToken = googleUser.getAuthResponse().id_token;
    document.cookie = 'Authorization=google_' + idToken + '; path=/';
    window.location = '/profile.html';
}

function onRegistrationSuccess(googleUser) {
    $('#registrationErrorRow').hide();

    var email = googleUser.getBasicProfile().getEmail();
    console.log('Registering Google account: ' + email);
    var data = {};
    data['email'] = email;

    $.ajax({
        method: 'POST',
        contentType: 'application/json',
        url: '/api/user',
        async: false,
        data: JSON.stringify(data)
    }).done(function() {
        console.log('Registration complete for ' + email + '.  Logging in...');
        var idToken = googleUser.getAuthResponse().id_token;
        document.cookie = 'Authorization=google_' + idToken + '; path=/';
        window.location = '/profile.html';
    }).fail(function(jqXHR, textStatus) {
        console.log('POST /api/user request failed: ' + jqXHR.status);
        if (jqXHR.status == 401) {
            logout();
        }
        $('#registrationErrorMessage').text('Could not register a new account.  Perhaps an account with this email address already exists?');
        $('#registrationErrorRow').show();
    });
}

function onFailure(error) {
    $('#errorMessage').text(error);
    $('#loginErrorRow').show();
}

