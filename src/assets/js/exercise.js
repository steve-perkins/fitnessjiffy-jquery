$(function () {

    var exercisesPerformed = [];
    init();

    /** Sets up styling and event handler bindings, and perform initial data load from the server. */
    function init() {
        // Initialize the Datepicker widget
        $("#datepicker").val(parseCurrentDate());
        $("#datepicker").change(function() {
            loadExercisesPerformed($('#datepicker').val());
            loadRecentExercises($('#datepicker').val());
        });

        // Add event handling to the "Recent Exercises" add button
        $("#recentExercisesButton").click(function () {
            var exerciseId = $("#recentExercisesSelect").val();
            addExercisePerformed(exerciseId);
        });

        // Add event handling to the "Browse by Category" category selection
        $("#categorySelect").change(function () {
            updateExerciseCategory();
        });

        // Add event handling to the "Browse by Category" add button
        $("#browseExercisesButton").click(function() {
            var exerciseId = $("#exerciseSelect").find(":selected").val();
            addExercisePerformed(exerciseId);
        });

        // Add event handling to the "Search Exercises" button
        $("#searchExercisesButton").click(function() {
            searchExercises();
        });
        $("#searchExercisesName").keypress(function(e) {
            if (e.which == 13) {
                searchExercises();
            }
        });

        // Initial data load from the server
        loadExercisesPerformed(parseCurrentDate());
        loadRecentExercises(parseCurrentDate());
        loadExerciseCategories();
    }

    /** Retrieves information from the server about exercises performed on the currently selected date. */
    function loadExercisesPerformed(dateString) {
        $.ajax({
            method: 'GET',
            url: '/api/exerciseperformed/' + encodeURIComponent(dateString)
        }).done(function(payload) {
            exercisesPerformed = payload;
            renderExercisesPerformed();
        }).fail(function(jqXHR, textStatus) {
            console.log('GET /api/exerciseperformed/' + dateString + ' request failed: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
        });
    }

    /** Populates the exercises performed table with the data retrieved by "loadExercisesPerformed(...)" */
    function renderExercisesPerformed() {
        $('#exercisesPerformed tbody').empty();
        $('#exercisesPerformed tfoot').empty();
        var totalMinutes = 0;
        var totalCaloriesBurned = 0;

        var sortedExercisesPerformed = exercisesPerformed.sort(function(a, b) {
            if (a.exercise.description < b.exercise.description) {
                return -1;
            }
            if (a.exercise.description > b.exercise.description) {
                return 1;
            }
            return 0;
        });
        for (var index = 0; index < sortedExercisesPerformed.length; index++) {
            var exercisePerformed = sortedExercisesPerformed[index];
            totalMinutes += exercisePerformed.minutes;
            totalCaloriesBurned += exercisePerformed.caloriesBurned;

            $('#exercisesPerformed tbody').append('<tr id="' + exercisePerformed.id + '">'
                + '<td>' + exercisePerformed.exercise.description + '</td>'
                + '<td><input class="form-control" type="text" size="3" value="' + exercisePerformed.minutes + '"/></td>'
                + '<td>' + exercisePerformed.caloriesBurned + '</td>'
                + '<td><a href="#"><i class="uk-icon-trash"/></a></td></tr>'
                + '</tr>'
            );

            var minutesInput = $('#' + exercisePerformed.id).find('input');
            var deleteButton = $('#' + exercisePerformed.id).find('i.uk-icon-trash');
            minutesInput.on('change', minutesInput, updateExercisePerformed);
            deleteButton.on('click', deleteButton, deleteExercisePerformed);
        }
        $('#exercisesPerformed tfoot').append('<tr class="strong">'
            + '<td>TOTAL: </td>'
            + '<td>' + totalMinutes + '</td>'
            + '<td>' + totalCaloriesBurned + '</td>'
            + '<td></td>'
            + '</tr>'
        );
    }

    /** Adds a new exercise performed for the currently selected date. */
    function addExercisePerformed(exerciseId) {
        var dateString = $('#datepicker').val();
        $.ajax({
            method: 'POST',
            url: '/api/exerciseperformed',
            contentType: 'application/json',
            data: JSON.stringify({ id: exerciseId, date: dateString }),
            async: false
        }).done(function(exercisePerformedPayload) {
            exercisesPerformed.push(exercisePerformedPayload);
            renderExercisesPerformed();
        }).fail(function(jqXHR, textStatus) {
            console.log('An error occurred adding an exercise performed: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
        });
    }

    /** Called when the update icon is clicked on a row in the exercises performed table. */
    function updateExercisePerformed(event) {
        var input = event.data;
        var id = $(input).parent().parent().attr('id');
        var minutes = $(input).parent().parent().find('input').val();

        $.ajax({
            method: 'PUT',
            url: '/api/exerciseperformed/' + encodeURIComponent(id),
            contentType: 'application/json',
            data: JSON.stringify({ minutes: minutes })
        }).done(function(updatedExercisePerformed) {
            for (var index = 0; index < exercisesPerformed.length; index++) {
                if (exercisesPerformed[index].id == updatedExercisePerformed.id) {
                    exercisesPerformed[index] = updatedExercisePerformed;
                }
            }
            $(input).css('border', '2px solid green');
            setTimeout(function() {
                $(input).css('border', '0.666667px solid rgb(223, 215, 202');
                renderExercisesPerformed();
            }, 1000);
        }).fail(function(jqXHR, textStatus) {
            console.log('An error occurred updating an exercise performed: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
            $(input).css('border', '2px solid red');
        });
    }

    /** Called when the delete icon is clicked on a row in the exercises performed table. */
    function deleteExercisePerformed(event) {
        var input = event.data;
        var id = $(input).parent().parent().parent().attr('id');
        $.ajax({
            method: 'DELETE',
            url: '/api/exerciseperformed/' + encodeURIComponent(id)
        }).done(function() {
            for (var index = 0; index < exercisesPerformed.length; index++) {
                if (exercisesPerformed[index].id == id) {
                    exercisesPerformed.splice(index, 1);
                }
            }
            $(input).parent().parent().parent().css('border', '2px solid green');
            setTimeout(function() {
                $(input).parent().parent().parent().remove();
                renderExercisesPerformed();
            }, 500);
        }).fail(function(jqXHR, textStatus) {
            console.log('An error occurred deleting an exercise performed: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
            $(input).parent().parent().parent().css('border', '2px solid red');
        });
    }

    /**
     * Populates the "Recently Performed Exercises" select pull-down, when the page is first loaded and when
     * the date is changed.
     */
    function loadRecentExercises(dateString) {
        $.ajax({
            method: 'GET',
            url: '/api/exercise/recent/' + encodeURIComponent(dateString)
        }).done(function (recentExercisesPayload) {
            $('#recentExercisesSelect').empty();
            for (var index = 0; index < recentExercisesPayload.length; index++) {
                var recentExercise = recentExercisesPayload[index];
                var description = (recentExercise.description.length < 50)
                    ? recentExercise.description
                    : (recentExercise.description.substring(0, 47) + "...");
                $('#recentExercisesSelect').append('<option value="' + recentExercise.id + '">' + description + '</option>')
            }
        }).fail(function(jqXHR, textStatus) {
            console.log('An error occurred when loading recently performed exercises: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
        });

    }

    /** Populates the category select pull-down in the "Browse Exercises by Category" area. */
    function loadExerciseCategories() {
        $.ajax({
            method: 'GET',
            url: '/api/exercise/category'
        }).done(function(categories) {
            $('#categorySelect').empty();
            for (var index = 0; index < categories.length; index++) {
                var category = categories[index];
                $('#categorySelect').append('<option value="' + category + '">' + category + '</option>');
            }
            updateExerciseCategory();
        }).fail(function(jqXHR, textStatus) {
            console.log('GET /api/exercise/category request failed: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
        });
    }

    /**
     * When the category select pull-down changes, this function is called to populate the exercises select
     * pull-down with all of the exercises under that category.
     */
    function updateExerciseCategory() {
        var category = $("#categorySelect").val();
        $.ajax({
            method: 'GET',
            url: '/api/exercise/category/' + encodeURIComponent(category)
        }).done(function(exercises) {
            $("#exerciseSelect").empty();

            for (var index = 0; index < exercises.length; index++) {
                var exercise = exercises[index];
                var description = (exercise.description.length < 50) ? exercise.description : (exercise.description.substring(0, 47) + "...");
                var option = $("<option></option>").attr("value", exercise.id).text(description);
                $("#exerciseSelect").append(option);
            }
        }).fail(function(jqXHR, textStatus) {
            console.log('GET /api/exercise/category/ ' + category + ' request failed: ' + jqXHR.status);
            if (jqXHR.status == 401) {
                logout();
            }
        });
    }

    /** Displays a modal with search results. */
    function searchExercises() {
        var searchString = $("#searchExercisesName").val();
        $.ajax({
            method: 'GET',
            url: "/api/exercise/search/" + encodeURIComponent(searchString),
            dataType: "json",

            success: function(response) {
                $("#searchExercisesTable").empty();
                $("#searchExercisesTable").append("<tr><th>Name</th><th></th></tr>");
                for (var index = 0; index < response.length; index++) {
                    var exercise = response[index];
                    var description = (exercise.description.length <= 50) ? exercise.description : exercise.description.substring(0, 47) + "...";
                    var exerciseRow = "<tr><td>" + description + "</td>";
                    exerciseRow += "<td><a href='#' title='Add'><i id='exercise" + exercise.id + "'class='uk-icon-plus'/></a></td></tr>";
                    $("#searchExercisesTable").append(exerciseRow);

                    $('#exercise' + exercise.id).click(function(event) {
                        var id = $(event.target).attr('id').replace(/^exercise/, '');
                        addExercisePerformed(id);
                        UIkit.modal('#searchExercisesModal').hide();
                    });

                }
                UIkit.modal('#searchExercisesModal').show();
            },

            fail: function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            }
        });
    }

});

