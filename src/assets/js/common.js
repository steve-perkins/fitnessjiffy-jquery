/** Parse today's date in yyyy-mm-dd format. */
function parseCurrentDate() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    if (month < 10) {
        month = "0" + month;
    }
    var day = date.getDate();
    if (day < 10) {
        day = "0" + day;
    }
    return year + "-" + month + "-" + day;
}

/** Returns the value of any URL query parameter matching "name". */
function getRequestParam(name){
    if (name = (new RegExp('[?&amp;]' + encodeURIComponent(name) + '=([^&amp;]*)')).exec(window.location.search)) {
        return decodeURIComponent(name[1]);
    }
}

/** Logs the user out, by deleting the cookie storing their JWT token. */
function logout() {
    document.cookie = 'Authorization=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    var auth2 = gapi.auth2.getAuthInstance();
    if (auth2.isSignedIn.get()) {
        auth2.signOut().then(function () {
            window.location = '/login.html?logout=true';
        });
    } else {
        window.location = '/login.html?logout=true';
    }
}

$(function() {
    $('#logout, #logout-offcanvas').click(function() {
        logout();
    });
});


// Google Sign-In functions (must be in global scope)

function initGoogleAuth() {
    gapi.load('auth2', function() {
        gapi.auth2.init();
    });
}
